# Внимание
Проект screensquid-compose переведен в архив. В будущем возможно вновь сделаю docker-image для развертывания screensquid. 
Однако, в данный момент тут будет архивная версия screensquid, которая была упакована в docker. 

Пожалуйста, по возможности не используйте compose-версию. Разверните полноценный screensquid по инструкции отсюда: https://sourceforge.net/projects/screen-squid/
В данном compose содержится множество нюансов, которые я не учитывал во время создания данного проекта. 

Спасибо за понимание. 
--------------------------------------




# Screensquid-Compose, или же screensquid для ленивых
Итак, вы решили поставить screensquid. Однако, вам не хочется разворачивать базу данных, настраивать все, и конечно же поднимать Apache/Nginx с нуля? 
Как раз для этих целей я и создал данный репозиторий.
В проде использовать не рекомендую.... Да хотя кого я обманываю, у нас работает на проде, и никаких проблем нет.

## Немного о screensquid
Screensquid - это набор скриптов, позволяющий проводить аналитику трафика у прокси-сервера squid.
Автор screensquid - break-people.ru (http://break-people.ru/cmsmade/?page=scriptology_screen_squid)
Я же просто странный админ, который любит запихивать сервисы в докер. 

## А зачем в докер? 
В принципе, логика тут в более быстром развертывании screensquid. 
Возможно кто-то хотел бы попробовать данный инструмент, но никак не доходили руки развернуть его у себя. 
При помощи данного compose вы можете все развернуть за пару минут. А если решите убрать - все удаляется за те же пару минут.

Если же вы решили запустить это в проде, то это довольно спорное решение. Докер не панацея, да и образы все таки жрут немало места.
Также усложняется процесс администрирования БД Postgres. Вы же иногда чистите её, верно? 
В любом случае, если исключить эти факторы - дерзайте. 

## Установка
* В связи с возросшим количеством обращений о том, что ничего не работает, пожалуйста, не забывайте о том, что надо склонировать весь проект
### Клонирование проекта 
1. Заходим в `/opt/`
2. Пишем `git clone https://gitlab.com/dimonleonov/screensquid-compose`
3. Заходим в папку `screensquid-compose` и проверяем, что все файлы на месте (*обязательно: у вас должен быть не только yml, но и все остальные папки, которые в этом репо*)
5. Поздравляю, вы замечательны, теперь можем продолжить.
### Установка самого screensquid.

1. Запускаем контейнеры при помощи `docker-compose up`. Будет поднят nginx и postgres. Для Nginx будет открыт 80 порт.
2. Заходим на http://host
3. Ставим нужный язык, нажимаем continue
4. Выставляем настройки: 
- Database Type: PostgreSQL 9
- Proxy name: Любое название
- Database name: screensquid
- Username: postgres
- Password: postgres
- Database host address: postgres
- Squid host: IP_вашей_прокси
5. Нажимаем Continue
6. Поздравляю, вы замечательны. 

## Контексты использования
#### Локальный 
Вы можете держать squid и screensquid на одной машине. Однако если ресурсов мало, ваша прокси может тупить.
Для автозагрузки логов нужно добавить crontab:
1. Редактируем crontab - `crontab -e`
2. Вставляем строку `0 23 * * * bash /opt/screensquid-compose/logs_local.sh` в crontab, после чего сохраняем обновленный crontab.



#### SS отдельно (ужасный костыль)
Это достаточно костыльный метод. Прошу обратить внимание, что такое делать имеет смысл, только если вы понимаете, что делаете.
Вы можете держать screensquid на отдельной виртуалке/машине. Это позволит освободить ресурсы машины со squid-ом. Особенно полезно, если трафика много, и в процессе работы SS с СУБД у вас заметно загружает процессор. 
Итак, для настройки по такому варианту вам придется сделать несколько дополнительных действий.
1. Генерируем ssh-ключи на сервере, на котором будете запускать контейнер: `ssh-keygen`
2. Копируем ssh-id на сервер squid (`ssh-copy-id user@10.0.0.1`. где 10.0.0.1 - ip вашего сервера squid. user - имя пользователя, от имени которого можно подключиться по ssh.)
3. Редактируем файл logs_grabber.sh (там все расписано в комментариях)
4. Копируем файл logs_grabber.sh куда нибудь. Например, в /opt/screensquid-compose/logs_grabber.sh
5. Редактируем crontab - `crontab -e`
6. Вставляем строку `0 23 * * * bash /opt/screensquid-compose/logs_grabber.sh` в crontab, после чего сохраняем обновленный crontab.

В чем суть: скрипт загружает логи каждую ночь с squid-сервера и помещает их в /var/log/squid на нынешнем сервере.
Эти логи подхватывает fetch.pl в контейнере fetch. Дальше fetch.pl загружает логи в screensquid. 

Внимание: у пользователя, под которым вы будете пытаться почитать логи squid-а должна группа `proxy`, иначе вы будете получать ошибку `scp: /var/log/squid/access.log: Permission denied`

## А как залить логи в ScreenSquid прямо после запуска?
Для загрузки лога достаточно ввести команду `docker exec -it screensquid_docker_fetch_1 ./fetch.pl` на сервере, на котором вы развернули screensquid.


# Внимание, костыли
Обратите внимание на то, что cron в контейнере "fetch" пока что работает достаточно криво 

# У меня что-то не работает, к кому я могу обратиться
Не стесняйтесь спрашивать, если вдруг возникнет проблема. 
* Вы можете создать issue с проблемой
* Вы можете написать в телегу: https://t.me/llunvedd 
