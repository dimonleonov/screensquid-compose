FROM nginx:latest
COPY ./html /var/www/html
COPY ./conf /etc/nginx	
RUN apt update
RUN mkdir /run/php
RUN apt install -y php7.4-fpm php7.4-pgsql php7.4-mysql php7.4-gd
COPY ./conf_php/www.conf /etc/php/7.4/fpm/pool.d/www.conf
RUN chown -R www-data:www-data /var/www/html/

CMD php-fpm7.4 && nginx -g 'daemon off;' # run nginx in foreground
